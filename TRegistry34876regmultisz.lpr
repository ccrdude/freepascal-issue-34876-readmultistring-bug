program TRegistry34876regmultisz;

uses
   Windows,
   Classes,
   Registry;

const
   MultiLineHelloWorld: WideString = 'Hello World' + #0 + 'Hallo Welt' + #0#0;

var
   reg: TRegistry;
   iApiResult: LONG;
   dwLen: DWord;
   pData: Pointer;
   sl: TStringList;
begin
   WriteLn('[i] FPC: ', {$I %FPCVERSION%}, ', Target CPU: ', {$I %FPCTARGETCPU%});
   reg := TRegistry.Create;
   try
      reg.RootKey := HKEY_CURRENT_USER;
      if reg.OpenKey('\QA\mantis-34876\', True) then begin
         try
            reg.DeleteValue('TestMultiLine');
            if reg.ValueExists('TestMultiLine') then begin
               WriteLn('[-] pre-test cleanup failed');
               Exit;
            end;
            dwLen := Length(MultiLineHelloWorld) * SizeOf(widechar);
            pData := Pointer(@MultiLineHelloWorld[1]);
            iApiResult := RegSetValueExW(reg.CurrentKey, 'TestMultiLine', 0, REG_MULTI_SZ, pData, dwLen);
            if (iApiResult <> ERROR_SUCCESS) then begin
               WriteLn('[-] setting test value failed');
               Exit;
            end;
            sl := TStringList.Create;
            try
               reg.ReadStringList('TestMultiLine', sl);
               if sl.Count = 2 then begin
                  WriteLn('[+] number of returned lines are as expected: ', sl.Count);
               end else begin
                  WriteLn('[-] number of returned lines fail: ', sl.Count);
                  Exit;
               end;
            finally
               sl.Free;
            end;
         finally
            reg.CloseKey;
         end;
      end;
   finally
      reg.Free;
   end;
end.
