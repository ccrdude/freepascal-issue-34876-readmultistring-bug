# Summary

This test project demonstrates the ReadMultiString bug in FreePascal, which returns on list item per character, as described in [issue 34876](https://bugs.freepascal.org/view.php?id=34876).

## Background

TRegistry.ReadStringList calls GetData with a standard `String`, cast to `PChar` as buffer, which by default is `AnsiString`/`PAnsiChar` (`Registry.pas`,  line 431).
```
Var
  Data: string;
// ... the following line is responsible for UTF-16 being stored in String=AnsiString:
     ReadDataSize := GetData(Name,PChar(Data),Info.DataSize,Info.RegData);
```

GetData calls SysGetData (`Registry.pas`,  line 280). 

```
function TRegistry.GetData(const Name: string; Buffer: Pointer; BufSize: Integer; out RegData: TRegDataType): Integer;
begin
  Result:=SysGetData(Name,Buffer,BufSize,RegData);
  If (Result=-1) then
    Raise ERegistryException.CreateFmt(SRegGetDataFailed, [Name]);
end;
```

SysGetData then calls ´RegQueryValueExW` to query the registry, which is the unicode version returning UTF-16 encoded text into the buffer. 

```
function TRegistry.SysGetData(const Name: String; Buffer: Pointer;
          BufSize: Integer; Out RegData: TRegDataType): Integer;
Var
  u: UnicodeString;
  RD : DWord;

begin
  u := UTF8Decode(Name);
  FLastError:=RegQueryValueExW(fCurrentKey,PWideChar(u),Nil,
                      @RD,Buffer,lpdword(@BufSize));
```

## Workarounds

My patch on the bugtracker fixes the passed data type, and encodes it as UTF-8 before assigning to TStrings.Text, since other text related TRegistry functions do also encode as UTF-8. Thaddy de Koning correctly commented that the RTL shouldn't be UTF-8, but this affects all existing functions as well.

Alternatives would be to update TRegistry to use UnicodeString everywhere, or have overloaded methods accepting UnicodeString.

## Test Output

###  Lazarus r60091, Free Pascal 3.2.0-beta-r40963
This version shows the described behaviour:
```
[i] FPC: 3.2.0, Target CPU: i386
[-] number of returned lines fail: 25
```

###  Lazarus r60403, Free Pascal Compiler version 3.3.1-r41293
Still fails:
```
[i] FPC: 3.3.1, Target CPU: i386
[-] number of returned lines fail: 25
```